<?php

namespace App\DataFixtures;

use App\Entity\Statistics;
use App\Entity\Teachr;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void {
        $teachrs = [
            ['name' => 'Bérenger', 'date'  => '2015-08-28'],
            ['name' => 'Vincent', 'date' => '2019-05-13'],
            ['name' => 'Matilde', 'date' => '2001-01-08'],
            ['name' => 'Charlotte', 'date' => '1998-06-15'],
            ['name' => 'Marina', 'date' => '2003-09-19'],
            ['name' => 'Patrick', 'date' => '2005-10-23'],
            ['name' => 'Pierre', 'date' => '2009-03-02'],
            ['name' => 'Sophie', 'date' => '2020-12-31'],
        ];

        foreach($teachrs as $t)  {
            $teachr = new Teachr();
            $teachr->setName($t['name'])
                ->setDate(DateTime::createFromFormat('Y-m-d', $t['date']));
            $manager->persist($teachr);
        }
        $manager->flush();

        $stats = new Statistics();
        $stats->setCount(0);
        $manager->persist($stats);
        $manager->flush();
    }
}
