<?php

namespace App\Controller;

use DateTime;
use App\Entity\Teachr;
use App\Entity\Statistics;
use App\Repository\TeachrRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TeachrController extends AbstractController {

    private $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    /**
     * @Route("/api/teachrs", name="get_teachrs", methods={"GET"})
     * @Route("/api/teachrs/{id}", name="get_teachr", methods={"GET"})
     * @return Response
     */
    public function getTeachrs(int $id = null): Response {
        if (is_null($id)){
            $teachrs = $this->manager->getRepository(Teachr::class)->findAll();
        }
        else{
            $teachrs = $this->manager->getRepository(Teachr::class)->find($id);
        }
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($teachrs, 'json');
        return new Response($res);
    }

    /**
     * @Route("/api/teachrs/{id}", name="put_teachrs", methods={"PUT"})
     * @return Response
     */
    public function putTeachrs(int $id, Request $request): Response {
        $data = json_decode($request->getContent(), false);
        if(is_null($data)) {return new Response('undefined teachr');}
        $teachr = $this->manager->getRepository(Teachr::class)->find($id);
        $teachr->setName($data->name);
        $teachr->setDate(DateTime::createFromFormat("Y-m-d", $data->date));
        $this->manager->persist($teachr);
        $this->manager->flush();   
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($teachr, 'json');
        return new Response($res);
    }

    /**
     * @Route("/api/teachrs", name="post_teachrs", methods={"POST"})
     * @return Response
     */
    public function postTeachrs(Request $request): Response {
        $data = json_decode($request->getContent(), false);
        if(is_null($data)) {return new Response('undefined teachr');}
        $teachr = new Teachr();
        $teachr->setName($data->name);
        $teachr->setDate(DateTime::createFromFormat("Y-m-d", $data->date));
        $this->manager->persist($teachr);
        $stat = $this->manager->getRepository(Statistics::class)->find(1);
        $stat->setCount($stat->getCount()+1);
        $this->manager->persist($stat); 
        $this->manager->flush();
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($teachr, 'json');
        return new Response($res);
    }
}