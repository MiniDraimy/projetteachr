import * as React from 'react';
import {
  Text, 
  View,
  Image,
  Pressable,
  StyleSheet,
  SafeAreaView } from 'react-native';

import Carousel from 'react-native-snap-carousel';

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
    marginRight: 30,
  },
  button1: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    borderRadius: 5,
    borderColor: 'black',
    elevation: 3,
    color: 'white',
    backgroundColor: 'blue',
  },
  button2: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'red',
    elevation: 3,
    color: 'red',
    backgroundColor: 'white',
  },
  text1: {
    fontSize: 15,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: 'white',
    flexDirection: "row",
  },
  text2: {
    fontSize: 15,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: 'red',
    flexDirection: "row",
  },

});

export default class App extends React.Component {


    constructor(props){
        super(props);
        this.state = {
          activeIndex:0,
          carouselItems: [
          {
              title:"Jeanne Geane",
              
              text1: "Joueuse de flûte",
              image: 'https://reactnative.dev/img/tiny_logo.png',            
          },
          {
            title:"Jean Delacroix",
            
            text1: "Joueur de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Melissa Mole",
            
            text1: "Joueuse de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Fiacre Abril",
            
            text1: "Joueuse de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Océane Piedalu",
            
            text1: "Joueuse de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Marie Lacombe",
            
            text1: "Joueuse de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Julie Loiselle",
            
            text1: "Joueuse de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Tabor Cotuand",
            
            text1: "Joueur de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Luc Durepos",
            
            text1: "Joueur de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
          {
            title:"Rive Bonenfant",
            
            text1: "Joueur de flûte",
            image: 'https://reactnative.dev/img/tiny_logo.png',
          },
        ]
      }
    }

    _renderItem({item,index}){
        return (
          <View style={{
              backgroundColor:'white',
              shadowOpacity: 10,
              shadowRadius: 10,
              shadowOffset: {width: 0, height:0},
              borderRadius: 5,
              height: 500,
              width: 300,
              padding: 30,
              marginTop: 50,
              marginLeft: 10,
              }}>
            <View style={{
              marginBottom: 40,
              flexDirection: "row",
            }}>
              <Image style={styles.tinyLogo} source={{uri: item.image}}/>  
              <Text style={{fontSize: 20, alignContent: "center"}}>{item.title} </Text>
            </View>
            <View style={{
              marginBottom: 25,
            }}>
            <Text style={{color: "gray"}}>Formation: </Text>
            <Text style={{fontSize: 15, fontWeight: "bold"}}>{item.text1}</Text>
            </View>
            <View style={{
              marginBottom: 50,
            }}>
            <Text style={{color: "gray"}}>Description: </Text>
            <Text style={{fontSize: 15, fontWeight: "bold", alignContent: "flex-end"}}>Calme et juste, je sais m'adapter
            à l'élève et comprendre sa méthode d'apprentissage afin de l'aider à progresser au mieux</Text>
            </View>
            <View style={{
              marginBottom: 15,
            }}>
            <Pressable style={styles.button1}>
            <Text style={styles.text1}>Prendre un cours avec ce Teach'r</Text>
            </Pressable>
            </View>
            <View>
            <Pressable style={styles.button2}>
            <Text style={styles.text2}>Retirer ce Teach'r de mes favoris</Text>
            </Pressable>
            </View>
          </View> 

        )
    }

    render() {
        return (
          <SafeAreaView style={{flex: 2, backgroundColor:'white'}}>
            <View style={{backgroundColor:'#334EFF', height: 250}}> 
              <Text style={{color: 'white', fontSize: 40, fontWeight: 'bold', paddingTop: 150, paddingLeft: 25}}> Teach'rs favoris</Text>
            </View>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', }}>
                  <Carousel
                  layout={"default"}
                  ref={ref => this.carousel = ref}
                  data={this.state.carouselItems}
                  sliderWidth={400}
                  itemWidth={325}
                  renderItem={this._renderItem}
                  onSnapToItem = { index => this.setState({activeIndex:index}) } />
            </View>
          </SafeAreaView>
        );
    }
}

